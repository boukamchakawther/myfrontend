import { LOGIN_USER, LOGOUT_USER, REFRESH_USER } from "./types"
import UserService from "../services/user.service"

export const login = (data) => async (dispatch) => {
    try {
      
        const res = await UserService.login(data);
        dispatch({
            type: LOGIN_USER,
            payload: res.data,
        });
        return Promise.resolve(res.data);
    } catch (err) {
        return Promise.reject(err);
    }
};
export const logout = (data) => async (dispatch) => {
    try {
        const res = await UserService.logout(data);
        dispatch({
            type: LOGOUT_USER,
            payload: res.data,
        });
        return Promise.resolve(res.data);
    } catch (err) {
        return Promise.reject(err);
    }
};
export const refresh_token = (data) => async (dispatch) => {
    try {
        const res = await UserService.refreshToken(data);
        dispatch({
            type: REFRESH_USER,
            payload: res.data,
        });
        return Promise.resolve(res.data);
    } catch (err) {
        return Promise.reject(err);
    }
};
