import { LOGIN_USER, LOGOUT_USER, REFRESH_USER } from "../actions/types"
//const initialState = [];
function UserReducer(USERS = [], action) {
    const { type, payload } = action;
    switch (type) {
        case LOGIN_USER:
            return [...USERS, payload];
        case LOGOUT_USER:
            return payload;
        case REFRESH_USER:
            return payload;
        default:
            return USERS;
    }
}
export default UserReducer;
