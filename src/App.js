import logo from "./logo.svg";
import "./App.css";

import Login from "./components/login.component";
//import User from './components/user.component'
import Home from "./components/home/home.component";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

function App() {  

  
  return (
    <Router>
      <Routes>
        <Route path="" element={<Home />} />
        <Route path="login" element={<Login />} />
      </Routes>
    </Router>
  );
}

export default App;
