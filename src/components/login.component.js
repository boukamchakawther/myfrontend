import React, { useState, useEffect } from "react";

import { connect, useDispatch } from "react-redux";
import { login } from "../actions/user.action";
function Login(props) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();

  useEffect(() => {
    return () => {};
  }, []);

  const signin = (e) => {
    e.preventDefault();
    console.log({ email: email, password: password });
    dispatch(login({ email: email, password: password }))
      .then((res) => {
        console.log(res)
        if (res.data !=null)
       window.location.href = "/";
      
      else
    alert('password or email invalid')
  })

      .catch((err) => {
        console.log(err);
      });

       

      
  };

  return (
    <>
      <div className="login-container lightmode">
        <div className="login-box animated fadeInDown">
          <div className="login-logo"></div>
          <div className="login-body">
            <div className="login-title">
              <strong>Log In</strong> to your account
            </div>
            <form className="form-horizontal" onSubmit={signin}>
              <div className="form-group">
                <div className="col-md-12">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="E-mail"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </div>
              </div>
              <div className="form-group">
                <div className="col-md-12">
                  <input
                    type="password"
                    className="form-control"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </div>
              </div>
              <div className="form-group">
                <div className="col-md-6">
                  <a href="#" className="btn btn-link btn-block">
                    Forgot your password?
                  </a>
                </div>
                <div className="col-md-6">
                  <button className="btn btn-info btn-block">Log In</button>
                </div>
              </div>
              <div className="login-or">OR</div>
              <div className="form-group">
                <div className="col-md-4">
                  <button className="btn btn-info btn-block btn-twitter">
                    <span className="fa fa-twitter"></span> Twitter
                  </button>
                </div>
                <div className="col-md-4">
                  <button className="btn btn-info btn-block btn-facebook">
                    <span className="fa fa-facebook"></span> Facebook
                  </button>
                </div>
                <div className="col-md-4">
                  <button className="btn btn-info btn-block btn-google">
                    <span className="fa fa-google-plus"></span> Google
                  </button>
                </div>
              </div>
              <div className="login-subtitle">
                Don't have an account yet? <a href="#">Create an account</a>
              </div>
            </form>
          </div>
          <div className="login-footer">
            <div className="pull-left">&copy; 2014 AppName</div>
            <div className="pull-right">
              <a href="#">About</a> |<a href="#">Privacy</a> |
              <a href="#">Contact Us</a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
const mapStateToProps = (state) => {
  return {
    USERS: state.USERS,
  };
};
export default connect(mapStateToProps, {
  login,
})(Login);
