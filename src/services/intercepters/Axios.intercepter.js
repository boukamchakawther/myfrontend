import axios from 'axios'
import UserService from "../user.service"
const AxiosInstance = axios.create({
    baseURL: "http://localhost:3000",
    timeout: 20000,
})
AxiosInstance.interceptors.request.use(function (config) {
    const token = localStorage.getItem("token");
    config.headers.Authorization = token;
    return config;
});
AxiosInstance.interceptors.response.use(async (response) => {
    console.log("INTERCEPTOR_RESPONSE", response)
    return response;
}, async (error) => {
    console.log("INTERCEPTOR", error)
    const originalRequest = error.config;
    if (!error.response) {
        return Promise.reject('Network Error')
    }
    else if ((error.response.status === 401) && !originalRequest._retry) {
        // refresh token
        const refresh_token = localStorage.getItem("refresh_token");
        const res_data = await UserService.refreshToken(refresh_token)
        localStorage.setItem('token', res_data.token);
        localStorage.setItem('refresh_token', res_data.refreshToken);

        return Promise.reject('Not Auth');

    } else {
        return error.response
    }
})

export default AxiosInstance
