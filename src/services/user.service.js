import AxiosInstance from "../services/intercepters/Axios.intercepter"
class UserService {
    login(data) {

        return AxiosInstance.post("/users/login", data)
    }
    logout(data) {

        return AxiosInstance.post("/users/logout", data)
    }
    refreshToken(data) {
        return AxiosInstance.post("/users/refreshtoken", data)
    }
}
export default new UserService();